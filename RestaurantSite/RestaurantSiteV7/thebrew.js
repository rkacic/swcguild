
function checkInfo () {
	var name = document.getElementById('nameInput').value;
	var email = document.getElementById('emailInput').value;
	var phone = document.getElementById('phoneInput').value;
	var check = true; //validates phone and email true
	var checkEmail = true; //validates email filled
	var checkPhone = true; //validates phone filled
	var selected = document.getElementById('selector').value;
	var info = document.getElementById('addInfo').value;
	var bestDay = false;
	var checks = document.forms[0];
	if (email == "") {
		checkEmail = false;
	};
	if (phone == "") {
		checkPhone = false;
	} else if (isNaN(phone)) {
		return alert("Oops! Looks like you need to enter a valid 10 digit phone number.")
	};
	if (checkEmail || checkPhone) {
		check = true;
	} else {
		check = false;
	};
	if (name == "" && check === false) {
		return alert("Further info is required! Please verify that you have input your name, email and or phone number, and atleast one day has been selected for best contact. Thank you.");
	};
	if (selected === "other" && info == "") {
		return alert("Please give more details on your choice of other so we may better serve you!");
	};
	for (var i = 7; i < checks.length; i++) {
		if (checks[i].checked) {
			bestDay = true;
		};
	};
	if (bestDay === false) {
		return alert("Please choose atleast one day for best contact. Thank you!")
	};
};
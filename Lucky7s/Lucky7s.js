function showTable() {
	document.getElementById('stats').style.visibility = "visible";
};
function playGame() {
	var totalWins = 0;
	var rollCount = 0;
	var wins = 0;
	var winCount = 0;
	var pot = document.getElementById('amountBet').value;
	var status = true;
	var warning = "you need to bet a minimum $1!";
	checkAmountBet();
//varify amount input to bet is number > 0
	function checkAmountBet() {
		if (isNaN(pot) || pot < 1 )  {
			return alert(warning);
		}
		else{
			document.getElementById('startingBet').innerHTML = "$"+pot;
			runTheNumbers();
			showTable();
		}
	};
//amount bet >= 1 roll dice play game out
	function runTheNumbers(){
		while (pot >= 1) {
			var die1 = Math.floor(Math.random()*6 +1);
			var die2 = Math.floor(Math.random()*6 +1);
			var dieRoll = die1 + die2;

			if (dieRoll == 7) {
				pot += 4;
				wins += 1;
				rollCount +=1;
				totalWins +=1;
				if (wins > winCount) {
					winCount = wins;
				}
			} else {
				pot -= 1;
				rollCount += 1;
				wins = 0;
			};
		};
		var winnings = winCount * 4;
//print results to table
		document.getElementById('totalRolls').innerHTML = rollCount;
		document.getElementById('highestAmount').innerHTML = "$"+winnings;
		document.getElementById('rollHighestAmount').innerHTML = winCount;
		document.getElementById('button').value = 'Play Again!';
//reset placeholder to 0.00 input for bet amount
		// function reset();
// This is for testing
	// console.log("Roll Count: " + rollCount);
	// console.log("Winnings: " + winnings);
	// console.log("WinCount: " + winCount);
	// console.log("Total Wins: " + totalWins);
	// console.log("Pot: " + pot);
	};
};